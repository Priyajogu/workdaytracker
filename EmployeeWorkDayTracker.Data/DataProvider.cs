﻿using EmployeeWorkDayTracker.Data.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Data
{
    public class DataProvider : IDataProvider
    {
        public List<BaseEmployee> AllEmployees { get; set; }

        public List<BaseEmployee> GetAllEmployees()
        {
            return AllEmployees;
        }

        public BaseEmployee GetEmployeeById(int employeeId)
        {
            return AllEmployees.Where(emp => emp.EmployeeID == employeeId).FirstOrDefault();
        }

        public void LoadEmployees()
        {
            var dummyData = new DummyEmployeeData();
            AllEmployees = new List<BaseEmployee>();

            int sequence = 1;
            foreach (var employee in dummyData.GetThirtyEmployees())
            {
                if (sequence == 1)
                {
                    sequence = 2;
                    AllEmployees.Add(new SalariedEmployee(employee.EmployeeID, employee.FirstName, employee.LastName, DesignationEnum.Manager));
                }
                else if (sequence == 2)
                {
                    sequence = 3;
                    AllEmployees.Add(new SalariedEmployee(employee.EmployeeID, employee.FirstName, employee.LastName, DesignationEnum.NonManager));
                }
                else if (sequence == 3)
                {
                    sequence = 1;
                    AllEmployees.Add(new HourlyEmployee(employee.EmployeeID, employee.FirstName, employee.LastName));
                }
            }

        }
    }

    public class DummyEmployeeData
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<DummyEmployeeData> GetThirtyEmployees()
        {
            var data = "[{\"EmployeeID\":0,\"FirstName\":\"Cervantes\",\"LastName\":\"Durham\"},{\"EmployeeID\":1,\"FirstName\":\"Beck\",\"LastName\":\"Phelps\"},{\"EmployeeID\":2,\"FirstName\":\"Harmon\",\"LastName\":\"Gibson\"},{\"EmployeeID\":3,\"FirstName\":\"Joseph\",\"LastName\":\"Salas\"},{\"EmployeeID\":4,\"FirstName\":\"Gillespie\",\"LastName\":\"Merritt\"},{\"EmployeeID\":5,\"FirstName\":\"Ayers\",\"LastName\":\"Gomez\"},{\"EmployeeID\":6,\"FirstName\":\"Jenna\",\"LastName\":\"Edwards\"},{\"EmployeeID\":7,\"FirstName\":\"Florine\",\"LastName\":\"Vega\"},{\"EmployeeID\":8,\"FirstName\":\"Hensley\",\"LastName\":\"Justice\"},{\"EmployeeID\":9,\"FirstName\":\"Lucille\",\"LastName\":\"Jacobs\"},{\"EmployeeID\":10,\"FirstName\":\"Rosario\",\"LastName\":\"Jensen\"},{\"EmployeeID\":11,\"FirstName\":\"Vicki\",\"LastName\":\"Mclean\"},{\"EmployeeID\":12,\"FirstName\":\"Mia\",\"LastName\":\"Noel\"},{\"EmployeeID\":13,\"FirstName\":\"Norman\",\"LastName\":\"Langley\"},{\"EmployeeID\":14,\"FirstName\":\"Boyle\",\"LastName\":\"Stanton\"},{\"EmployeeID\":15,\"FirstName\":\"Stone\",\"LastName\":\"Roach\"},{\"EmployeeID\":16,\"FirstName\":\"Stella\",\"LastName\":\"Warner\"},{\"EmployeeID\":17,\"FirstName\":\"Monique\",\"LastName\":\"Deleon\"},{\"EmployeeID\":18,\"FirstName\":\"Marion\",\"LastName\":\"Shepherd\"},{\"EmployeeID\":19,\"FirstName\":\"Juana\",\"LastName\":\"Dillard\"},{\"EmployeeID\":20,\"FirstName\":\"Mosley\",\"LastName\":\"Beard\"},{\"EmployeeID\":21,\"FirstName\":\"Herminia\",\"LastName\":\"Walsh\"},{\"EmployeeID\":22,\"FirstName\":\"Melva\",\"LastName\":\"Velez\"},{\"EmployeeID\":23,\"FirstName\":\"Morton\",\"LastName\":\"Norman\"},{\"EmployeeID\":24,\"FirstName\":\"Patrice\",\"LastName\":\"Hamilton\"},{\"EmployeeID\":25,\"FirstName\":\"Tasha\",\"LastName\":\"Sykes\"},{\"EmployeeID\":26,\"FirstName\":\"Pam\",\"LastName\":\"Flowers\"},{\"EmployeeID\":27,\"FirstName\":\"Martina\",\"LastName\":\"Travis\"},{\"EmployeeID\":28,\"FirstName\":\"Minnie\",\"LastName\":\"Garcia\"},{\"EmployeeID\":29,\"FirstName\":\"Gina\",\"LastName\":\"Bryant\"}]";
            return JsonConvert.DeserializeObject<List<DummyEmployeeData>>(data);
        }
    };
}

