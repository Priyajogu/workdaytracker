﻿using EmployeeWorkDayTracker.Data.Enum;
using System;

namespace EmployeeWorkDayTracker.Data
{
    public abstract class BaseEmployee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmployeeID { get; set; }
        public EmploymentTypeEnum EmployementType { get; set; }
        public DesignationEnum EmployeeDesignation { get; set; }
        public float WorkingDays { get; private set; } = 0;
        public float VacationDays { get; private set; } = 0;
        public float UtilizedVacationDays { get; private set; } = 0;
        public float VacationDayAccureFactor { get;  set; } 
            

        public virtual void Work(int workdays)
        {
            if (workdays > 0 && workdays <= 260  && (WorkingDays + workdays) <= 260)
            {
                WorkingDays += workdays;
                VacationDays = (WorkingDays * VacationDayAccureFactor) - UtilizedVacationDays;
            }
        }

        public virtual void TakeVacation(float takeVacationDays)
        {
            if (VacationDays > 0 && takeVacationDays < VacationDays)
            {
                UtilizedVacationDays += takeVacationDays;
                VacationDays -= takeVacationDays;
            }
        }
    }
}
