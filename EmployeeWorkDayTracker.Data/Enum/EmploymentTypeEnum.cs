﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Data.Enum
{
    public enum EmploymentTypeEnum
    {
        Hourly,
        Salaried
    }
}
