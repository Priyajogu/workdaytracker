﻿using System.Collections.Generic;

namespace EmployeeWorkDayTracker.Data
{
    public interface IDataProvider
    {
        List<BaseEmployee> GetAllEmployees();
        BaseEmployee GetEmployeeById(int employeeId);
    }
}