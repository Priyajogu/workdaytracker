﻿using EmployeeWorkDayTracker.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Data
{
    public class SalariedEmployee : BaseEmployee
    {  
        public SalariedEmployee(int employeeID, string firstName, string lastName, DesignationEnum employeeDesignation)
        {
            EmployeeID = employeeID;
            FirstName = firstName;
            LastName = lastName;
            EmployeeDesignation = employeeDesignation;
            EmployementType = EmploymentTypeEnum.Salaried;
            VacationDayAccureFactor = GetVacationDayAccureFactor();
        }

        private float GetVacationDayAccureFactor()
        {
            if(EmployeeDesignation == DesignationEnum.Manager)
            {
                return 30f/260f;
            }
            else
            {
                return 15f/260f;
            }
        } 
    }
}
