﻿using EmployeeWorkDayTracker.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EmployeeWorkDayTracker.Data
{
    public class HourlyEmployee : BaseEmployee
    {
        public HourlyEmployee(int employeeID, string firstName, string lastName)
        {
            EmployeeID = employeeID;
            FirstName = firstName;
            LastName = lastName;
            EmployeeDesignation = DesignationEnum.NonManager;
            EmployementType = EmploymentTypeEnum.Hourly;
            VacationDayAccureFactor = GetVacationDayAccureFactor();
        }

        private float GetVacationDayAccureFactor()
        {

            return 10f / 260f;
        }
    }
}
