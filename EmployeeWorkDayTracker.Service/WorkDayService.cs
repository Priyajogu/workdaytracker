﻿using EmployeeWorkDayTracker.Data;
using System;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Service
{
    public class WorkDayService : IWorkDayService
    {
        public WorkDayService(IDataProvider dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        private IDataProvider dataProvider { get; }

        public BaseEmployee AddWorkDays(int employeeId, int workDays)
        {
            var employee = dataProvider.GetEmployeeById(employeeId);
            employee.Work(workDays);
            return employee;
        }

        public BaseEmployee TakeVacation(int employeeId, float vacationDays)
        {
            var employee = dataProvider.GetEmployeeById(employeeId);
            employee.TakeVacation(vacationDays);
            return employee;
        }
    }
}
