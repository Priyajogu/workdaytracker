﻿using EmployeeWorkDayTracker.Data;
using System.Collections.Generic;

namespace EmployeeWorkDayTracker.Service
{
    public interface IEmployeeService
    {
        BaseEmployee GetEmployee(int employeeId);
        List<BaseEmployee> GetAllEmployees();
    }
}