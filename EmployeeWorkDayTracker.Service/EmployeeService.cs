﻿using EmployeeWorkDayTracker.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Service
{
    public class EmployeeService : IEmployeeService
    {
        private IDataProvider dataProvider;

        public EmployeeService(IDataProvider dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        public BaseEmployee GetEmployee(int employeeId)
        {
            var employee = dataProvider.GetEmployeeById(employeeId);
            return employee;
        }
        public List<BaseEmployee> GetAllEmployees()
        {
            return dataProvider.GetAllEmployees();
        }

    }
}
