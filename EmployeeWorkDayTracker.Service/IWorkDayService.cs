﻿using EmployeeWorkDayTracker.Data;
using System.Threading.Tasks;

namespace EmployeeWorkDayTracker.Service
{
    public interface IWorkDayService
    {
        BaseEmployee AddWorkDays(int employeeId, int workDays);
        BaseEmployee TakeVacation(int employeeId, float workDays);
    }
}