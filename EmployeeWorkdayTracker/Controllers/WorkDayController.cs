﻿using AutoMapper;
using EmployeeWorkdayTracker.WebApi.Objects;
using EmployeeWorkDayTracker.Data;
using EmployeeWorkDayTracker.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWorkdayTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/WorkDay")]
    public class WorkDayController : Controller
    {
        private readonly IWorkDayService workDayService;
        private readonly IMapper mapper;

        private ILogger<WorkDayController> logger { get; }
        public WorkDayController(ILogger<WorkDayController> logger, IWorkDayService workDayService, IMapper mapper)
        {
            this.logger = logger;
            this.workDayService = workDayService;
            this.mapper = mapper;
        }

        [HttpPost]
        [Route("AddWorkDays")]
        public ActionResult<EmployeeResponse> AddWorkDays(AddWorkDayRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }
                  return Ok(this.mapper.Map<BaseEmployee,EmployeeResponse>(workDayService.AddWorkDays(request.EmployeeId,request.WorkDays)));
            }
            catch (Exception ex)
            {
                logger.LogError(new { ex.Message,ex.StackTrace }.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
            
        }

        [HttpPost]
        [Route("TakeVacation")]
        public ActionResult<EmployeeResponse> TakeVacation(TakeVacationRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }
                 return Ok(this.mapper.Map<BaseEmployee, EmployeeResponse>(workDayService.TakeVacation(request.EmployeeId, request.VacationDays)));
            }
            catch (Exception ex)
            {
                logger.LogError(new { ex.Message, ex.StackTrace }.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            } 
        }
    }
}
