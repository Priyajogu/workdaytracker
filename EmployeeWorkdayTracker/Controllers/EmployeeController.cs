﻿using AutoMapper;
using EmployeeWorkdayTracker.WebApi.Objects;
using EmployeeWorkDayTracker.Data;
using EmployeeWorkDayTracker.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWorkdayTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/Employee")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService employeeService;
        private readonly IMapper mapper;

        private ILogger<EmployeeController> logger { get; }
        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeService employeeService, IMapper mapper)
        {
            this.logger = logger;
            this.employeeService = employeeService;
            this.mapper = mapper;
        }
        [HttpGet]
        [Route("GetAllEmployees")]
        public ActionResult<List<EmployeeResponse>> GetAllEmployees()
        {
            try
            {
                return mapper.Map<List<BaseEmployee>, List<EmployeeResponse>>(employeeService.GetAllEmployees());
            }
            catch (Exception ex)
            {
                logger.LogError(new { ex.Message, ex.StackTrace }.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpGet]
        [Route("GetEmployeeById")]
        public ActionResult<EmployeeResponse> GetEmployeeById(int employeeId)
        {
            try
            {
                return mapper.Map<BaseEmployee, EmployeeResponse>(employeeService.GetEmployee(employeeId: employeeId));
            }
            catch (Exception ex)
            {
                logger.LogError(new { ex.Message, ex.StackTrace }.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
