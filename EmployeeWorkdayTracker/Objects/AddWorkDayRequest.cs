﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWorkdayTracker.WebApi.Objects
{
    public class AddWorkDayRequest
    {
        [Required]
        public int WorkDays { get; set; }
        [Required]
        public int EmployeeId { get; set; }
    }
}
