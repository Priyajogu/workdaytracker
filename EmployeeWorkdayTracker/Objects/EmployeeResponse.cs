﻿using EmployeeWorkDayTracker.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWorkdayTracker.WebApi.Objects
{
    public class EmployeeResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmployeeID { get; set; }
        public EmploymentTypeEnum EmployementType { get; set; }
        public DesignationEnum EmployeeDesignation { get; set; }
        public float WorkingDays { get; set; }
        public float VacationDays { get; set; }
    }
}
